package homework6.Homework1

fun main() {
//    Вроде как в тайме сказали, что в ДЗ 1 не делать обработку исключений

    var user: User? = signUp("admin", "1234567890","1234567890")
    user?.printUser()
}

fun signUp(login: String, password : String, confirmPassword : String): User {
    if(login.length > 20 || login.length < 3) {
        throw WrongLoginException("В логине должно быть 3..20 символов: $login (${login.length})")
    }
    if(password.length < 10) {
        throw WrongPasswordException("В пароле не должно быть менее 10 символов: $password (${password.length})")
    }
    if(password!=confirmPassword) {
        throw WrongPasswordException("пароль и подтверждение пароля должны совпадать: $password не равен $confirmPassword")
    }
    return User(login,password)
}