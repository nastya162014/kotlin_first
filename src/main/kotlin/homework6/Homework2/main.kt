package homework6.Homework2

fun main() {
    val x = readLine()!!.toInt()
    try {
        exceptionTest(x)
    } catch (e: TestChildException) {
        println("${e.message}")
    } catch (e: TestParentException) {
        println("${e.message}")
    } catch (e: Exception) {
        println("${e.message}")
    }
}
fun exceptionTest(x: Int) {
    when (x) {
        1 -> throw TestChildException("TestChildException")
        2 -> throw TestParentException("TestParentException")
        else -> throw Exception("Exception")
    }
}