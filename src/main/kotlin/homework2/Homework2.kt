package homework2

fun main() {
    val soda: Short = 18500
    val pinaColada: Int = 200 //  val pinaColada: Short = 200
    val whiskey: Byte = 50
    val fresh: Long = 3000000000
    val cola: Float = 0.5F
    val ale: Double = 0.666666667
    val authorDrink: String = "Что-то авторское!"

    println("\"Заказ - \'$whiskey мл виски\' готов!\"")  //вариант вывода о котором писали в треде в тайме
    println("“Заказ - ‘$whiskey мл виски’ готов!”.")    //вариант вывода, когда сообщение с знаками скопировала как есть с Задания

    println("\"Заказ - \'$soda мл лимонада\' готов!\"")
    println("\"Заказ - \'$pinaColada мл пина колады\' готов!\"")
    println("\"Заказ - \'$fresh капель фреша\' готов!\"")
    println("\"Заказ - \'$cola литра колы\' готов!\"")
    println("\"Заказ - \'$ale литра эля\' готов!\"")
    println("\"Заказ - \'$authorDrink\' готов!\"")
}