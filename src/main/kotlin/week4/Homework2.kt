package week4

fun main() {
    findReverseNumber(876543210)
}

fun findReverseNumber(num: Int) {
    if(num>0) {
        print(num%10)
        findReverseNumber(num/10)
    }

}
