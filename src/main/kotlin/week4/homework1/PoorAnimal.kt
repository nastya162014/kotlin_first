package week4.homework1

abstract class PoorAnimal(val name: String, var weight: Float, var height: Float){
    abstract var eatingHabits: Array<String>

    var fullness: Int = 0
        set(value) {
            if (value > 0) {
                field = value
            }
        }

    fun eat(food: String){
        println("Food: "  + food)
        for (item in eatingHabits){
            if(item==food){
                fullness++
            }
        }
    }
}