package week4.homework1

class Wolf(name: String, weight: Float, height: Float): PoorAnimal(name, weight, height){
    override var eatingHabits: Array<String> = arrayOf("beef", "spicy wings")
}