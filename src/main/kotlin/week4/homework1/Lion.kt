package week4.homework1

class Lion(name: String, weight: Float, height: Float): PoorAnimal(name, weight, height){
    override var eatingHabits: Array<String> = arrayOf("beef", "nuggets")
}