package week4.homework1

fun main() {
    val lion = Lion("Ivan", 190F, 120F)
    println("Food of the Lion: "  + lion.eatingHabits.joinToString(", "))
    println("Fullness: "  + lion.fullness)
    lion.eat("beef")
    println("Fullness: "  + lion.fullness)
    lion.eat("banana")
    println("Fullness: "  + lion.fullness)

    val chimpanzee = Chimpanzee("Ivan", 190F, 120F)
    println("Food of the Chimpanzee: "  + chimpanzee.eatingHabits.joinToString(", "))
    println("Fullness: "  + chimpanzee.fullness)
    chimpanzee.eat("beef")
    println("Fullness: "  + chimpanzee.fullness)
    chimpanzee.eat("banana")
    println("Fullness: "  + chimpanzee.fullness)

}

