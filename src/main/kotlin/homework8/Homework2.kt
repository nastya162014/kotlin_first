package homework8

val userCart = mutableMapOf<String, Int>("potato" to 2, "cereal" to 2, "milk" to 1, "sugar" to 3, "onion" to 1, "tomato" to 2, "cucumber" to 2, "bread" to 3)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    countVegetables(userCart, vegetableSet)

    try {
        countTotalCartCost(userCart, discountSet, discountValue, prices)
    } catch (e: NoSuchElementException) {
        println("Не для всех товаров задана стоимость. Не удается посчитать общую стоимость корзины товаров.")
    }
}
fun countTotalCartCost(userCart: MutableMap<String, Int>,
                       discountSet: Set<String>,
                       discountValue: Double,
                       prices: MutableMap<String, Double>){
    var cost = 0.00
    for((key, value) in userCart) {
        if(discountSet.contains(key)){
            cost = cost + (value * (1 - discountValue) * prices.getValue(key))
        } else {
            cost = cost + (value * prices.getValue(key))
        }
    }
    println("Стоимость всех товаров: $cost")
}

fun countVegetables(userCart: MutableMap<String, Int>, vegetableSet: Set<String>) {
    var count = 0
    for((key, value) in userCart) {
        if(vegetableSet.contains(key)){
            count = count + value
        }
    }
    println("Количество овощей: $count")
}
