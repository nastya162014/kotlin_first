package homework8

fun main() {
    val sourceList = mutableListOf(1, 2, 3, 1, 2, 3, 1, 1, 5, 6, 8, 1, 8, 7, 4, 9)
    println(removeDuplicatesFromMutableList(sourceList))
}
fun removeDuplicatesFromMutableList(sourceList: MutableList<Int>): MutableList<Int> {
    return sourceList.toSet().toMutableList()
}
