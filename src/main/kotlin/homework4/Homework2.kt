package homework4

import java.util.*


fun main() {
    Locale.setDefault(Locale("en", "US"))
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    computePerformance(marks)
}
fun computePerformance(marks: Array<Int>){
    var fiveCount = 0
    var fourCount = 0
    var threeCount = 0
    var twoCount = 0
    for(mark in marks){
        when(mark) {
            2 -> twoCount++
            3 -> threeCount++
            4 -> fourCount++
            5 -> fiveCount++
        }
    }
    println("отличников - " + "%.1f".format(fiveCount*100F/marks.size) + "%")

    println("хорошистов - " + "%.1f".format(fourCount*100F/marks.size) + "%")

    println("троечников - " + "%.1f".format(threeCount*100F/marks.size) + "%")

    println("двоечников - " + "%.1f".format(twoCount*100F/marks.size) + "%")


}

