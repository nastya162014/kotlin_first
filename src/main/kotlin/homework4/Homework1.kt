package homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    countCoins(myArray)
}
fun countCoins(myArray: Array<Int>){
    var oddCount = 0
    var evenCount = 0
    for(item in myArray){
        if(item % 2 == 0){
            evenCount++
        }else{
            oddCount++
        }
    }
    println("Четные — Джо: $evenCount" )
    println("Нечетные — команда: $oddCount" )
}