package week5

class Giraffe(name: String, weight: Float, height: Float): Animal(name, weight, height){
    override var eatingHabits: Array<String> = arrayOf("tree leaves")
    override fun eat(food: String){
        println("Call fun from Giraffe class")
        if (food in eatingHabits){
            fullness++
        }
    }
}