package week5

class Lion(name: String, weight: Float, height: Float): Animal(name, weight, height){
    override var eatingHabits: Array<String> = arrayOf("beef", "nuggets")
    override fun eat(food: String){
        println("Call fun from Lion class")
        if (food in eatingHabits){
            fullness++
        }
    }
}