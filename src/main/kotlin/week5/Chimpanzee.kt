package week5

class Chimpanzee(name: String, weight: Float, height: Float): Animal(name, weight, height){
    override var eatingHabits: Array<String> = arrayOf("banana", "big bananas")
    override fun eat(food: String){
        println("Call fun from Chimpanzee class")
        if (food in eatingHabits){
            fullness++
        }
    }
}