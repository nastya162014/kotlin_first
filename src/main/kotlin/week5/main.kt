package week5

fun main() {
    val animals = arrayOf(
        Chimpanzee("Chimpanzee", 190F, 120F),
        Elephant("Elephant", 190F, 120F),
        Giraffe("Giraffe", 190F, 120F),
        Gorilla("Gorilla", 190F, 120F),
        Hippo("Hippo", 190F, 120F),
        Lion("Lion", 190F, 120F),
        Tiger("Tiger", 190F, 120F),
        Wolf("Wolf", 190F, 120F)
    )
    val eatingHabitsAllAnimals = arrayOf("beef", "nuggets", "spicy wings", "watermelon", "pumpkin", "banana", "tree leaves")

    feedAnimal(animals,eatingHabitsAllAnimals)
}

fun feedAnimal(animals: Array<Animal>, eatingHabitsAllAnimals: Array<String>){
    for (animal in animals){
        for (item in eatingHabitsAllAnimals){
            println("Food: "  + item + " for animal: " + animal.name)
            animal.eat(item)
            println("Fullness: " + animal.fullness)
        }
    }
}

