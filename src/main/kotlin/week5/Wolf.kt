package week5

class Wolf(name: String, weight: Float, height: Float): Animal(name, weight, height){
    override var eatingHabits: Array<String> = arrayOf("beef", "spicy wings")
    override fun eat(food: String){
        println("Call fun from Wolf class")
        if (food in eatingHabits){
            fullness++
        }
    }
}