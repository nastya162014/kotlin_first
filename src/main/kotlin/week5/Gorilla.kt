package week5

class Gorilla(name: String, weight: Float, height: Float): Animal(name, weight, height){
    override var eatingHabits: Array<String> = arrayOf("big bananas")
    override fun eat(food: String){
        println("Call fun from Gorilla class")
        if (food in eatingHabits){
            fullness++
        }
    }
}