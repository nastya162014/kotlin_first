package week5

class Tiger(name: String, weight: Float, height: Float): Animal(name, weight, height){
    override var eatingHabits: Array<String> = arrayOf("beef", "nuggets", "spicy wings")
    override fun eat(food: String){
        println("Call fun from Tiger class")
        if (food in eatingHabits){
            fullness++
        }
    }
}