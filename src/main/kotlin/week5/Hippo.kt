package week5

class Hippo(name: String, weight: Float, height: Float): Animal(name, weight, height){
    override var eatingHabits: Array<String> = arrayOf("watermelon", "pumpkin")
    override fun eat(food: String){
        println("Call fun from Hippo class")
        if (food in eatingHabits){
            fullness++
        }
    }
}