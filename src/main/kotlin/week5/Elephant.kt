package week5

class Elephant(name: String, weight: Float, height: Float): Animal(name, weight, height){
    override var eatingHabits: Array<String> = arrayOf("watermelon", "apple", "banana")
    override fun eat(food: String){
        println("Call fun from Elephant class")
        if (food in eatingHabits){
            fullness++
        }
    }
}