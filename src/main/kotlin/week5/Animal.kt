package week5

abstract class Animal(var name: String, var weight: Float, var height: Float){
    abstract var eatingHabits: Array<String>

    var fullness: Int = 0
         protected set(value) {
            if (value > 0) {
                field = value
            }
        }
//    fun eat(food: String){
//            if (food in eatingHabits){
//                fullness++
//        }
//    }
//    Сделаю метод абстрактным, чтобы показать явно полиморфизм , вроде именно это хотят в Задании:
//    Ребята просят вас реализовать функцию, которая с помощью полиморфизма покормит всех животных в зоопарке.
    abstract fun eat(food: String)
}